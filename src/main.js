// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import { routes } from './routes'
import 'bulma'

Vue.use(VueResource);
Vue.use(VueRouter);

Vue.config.productionTip = false

export const router = new VueRouter({
    routes
});

Vue.mixin({
    data: function() {
        return {
            get url_backend() {
                return "http://127.0.0.1:3000/";
            }
        }
    }
});

Vue.filter('cash', function(value) {
    return "$" + value
});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
