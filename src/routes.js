const Home = resolve => {
    require.ensure(['./components/MainContent.vue'], () => {
        resolve(require('./components/MainContent.vue'));
    });
};

const CoinList = resolve => {
    require.ensure(['./components/CoinList.vue'], () => {
        resolve(require('./components/CoinList.vue'));
    });
};


const CoinNames = resolve => {
    require.ensure(['./components/CoinNames.vue'], () => {
        resolve(require('./components/CoinNames.vue'));
    });
};


const CoinPrediction = resolve => {
    require.ensure(['./components/CoinPrediction.vue'], () => {
        resolve(require('./components/CoinPrediction.vue'));
    });
};


const ValueHistory = resolve => {
    require.ensure(['./components/ValueHistory.vue'], () => {
        resolve(require('./components/ValueHistory.vue'));
    });
};



export const routes = [
    { path: '', component: Home, name: 'home'},
    { path: '/coin-list', component: CoinList, name: 'coin-list'},
    { path: '/coin-names', component: CoinNames, name: 'coin-names'},
    { path: '/coin-prediction', component: CoinPrediction, name: 'coin-prediction'},
    { path: '/historical-value', component: ValueHistory, name: 'historical-value'},
];
