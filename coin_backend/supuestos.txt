0) El frontend está en el directorio principal, hecho en VueJS. Se inicia con npm run dev.
   El backend está en la carpeta coin_backend y está hecho en Ruby on Rails. Se inicia con rails server.
1) Nuestros usuarios son muy cuidados y entregan los datos en el formato correcto.
2) El servidor front-end estará en http://localhost:8080. Se configuró a los CORS según ese parámetro.
3) Se realiza la medición de cada una de las monedas de forma diaria.
4) El algoritmo de predicción toma los datos de la semana anterior y los promedia, dando la predicción.
5) Se pueden agregar más monedas editando el archivo de seeds de ruby o agregándolos directamente en la base de datos.
6) Se usan dos API:
    - Cryptomonedas: cryptocompare.com
    - Monedas regulares: http://apilayer.net
7) Todos los valores de monedas se entregan en comparación a 1 USD.