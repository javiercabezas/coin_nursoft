Rails.application.routes.draw do
  get '/', to: 'coin#index'
  get '/currencies', to: 'coin#list'
  get '/currency/:currency', to: 'coin#details'
  get '/predict/:currency', to: 'coin#predict'
end
