# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
coins = Coin.create([
                        { short_name: 'CLP', name: 'Chilean Peso', is_crypto: false },
                        { short_name: 'BTC', name: 'Bitcoin', is_crypto: true },
                        { short_name: 'DOGE', name: 'Dogecoin', is_crypto: true },
                        { short_name: 'MXN', name: 'Mexican Peso', is_crypto: false },
                        { short_name: 'ETH', name: 'Ethereum', is_crypto: true },
                        { short_name: 'ARS', name: 'Argentine Peso', is_crypto: false }
                    ])

CoinValue.create(date: '09-11-2017', price: 632.511, coin: coins[0])
CoinValue.create(date: '09-11-2017', price: 7182.29, coin: coins[1])
CoinValue.create(date: '09-11-2017', price: 0.001415, coin: coins[2])

CoinValue.create(date: '10-11-2017', price: 630.9148, coin: coins[0])
CoinValue.create(date: '10-11-2017', price: 6656.84, coin: coins[1])
CoinValue.create(date: '10-11-2017', price: 0.001163, coin: coins[2])

CoinValue.create(date: '11-11-2017', price: 631.7119, coin: coins[0])
CoinValue.create(date: '11-11-2017', price: 6413.51, coin: coins[1])
CoinValue.create(date: '11-11-2017', price: 0.001201, coin: coins[2])

CoinValue.create(date: '12-11-2017', price: 631.313, coin: coins[0])
CoinValue.create(date: '12-11-2017', price: 6221.45, coin: coins[1])
CoinValue.create(date: '12-11-2017', price: 0.001038, coin: coins[2])

CoinValue.create(date: '13-11-2017', price: 630.9148, coin: coins[0])
CoinValue.create(date: '13-11-2017', price: 5986.79, coin: coins[1])
CoinValue.create(date: '13-11-2017', price: 0.001211, coin: coins[2])

CoinValue.create(date: '14-11-2017', price: 630.119, coin: coins[0])
CoinValue.create(date: '14-11-2017', price: 6657.62, coin: coins[1])
CoinValue.create(date: '14-11-2017', price: 0.001184, coin: coins[2])

CoinValue.create(date: '15-11-2017', price: 632.911, coin: coins[0])
CoinValue.create(date: '15-11-2017', price: 6865.51, coin: coins[1])
CoinValue.create(date: '15-11-2017', price: 0.001339, coin: coins[2])