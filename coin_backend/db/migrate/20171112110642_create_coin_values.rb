class CreateCoinValues < ActiveRecord::Migration[5.1]
  def change
    create_table :coin_values do |t|
      t.date :date
      t.decimal :price, precision: 15, scale: 8
      t.timestamps
    end

    add_reference :coin_values, :coin, index: true
  end
end
