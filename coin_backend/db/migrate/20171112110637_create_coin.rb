class CreateCoin < ActiveRecord::Migration[5.1]
  def change
    create_table :coins do |t|
      t.string :short_name, :limit => 4
      t.string :name, :limit => 30, unique: true
      t.boolean :is_crypto
      t.timestamps
    end
  end
end
