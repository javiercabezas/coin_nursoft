class CoinController < ActionController::API
  #Returns an json array with all the coins list with the last value registered for it on the database.
  def index
    @coin_arr = []
    Coin.all.each do |coin|
      @coin_arr << coin.to_h
    end
    render json: @coin_arr
  end

  #Returns an json array with all the coins list registered on the database in the format 'coin_short_name' => Coin name
  def list
    coin_hash = {}
    Coin.all.each do |coin|
      coin_hash[coin.short_name] = coin.name
    end
    render json: coin_hash
  end

  #Returns the values of the coins for the given date interval
  #In case one of the dates is an empty string its uses the default value of from: the day one week ago and to: today
  def details
    if params['from'] == ""
      from_date = Date.today - 7
    else
      from_date = params['from'].to_date
    end

    if params['to'] == ""
      to_date = Date.today
    else
      to_date = params['to'].to_date
    end

    render json: CoinValue.fill_dates(params['currency'], from_date, to_date)
  end

  #Gets the average of the last 7 days and gives the value as a prediction for tomorrow.
  def predict
    from_date = Date.today - 7
    to_date = Date.today
    render json: CoinValue.predict(params['currency'])
  end
end
