require 'open-uri'

class CoinValue < ApplicationRecord
  URL_API_CRYPTO = 'https://min-api.cryptocompare.com/data/pricehistorical?'
  URL_API_REGULAR = 'http://apilayer.net/api/historical?access_key=2ed28776b5fffea4037db970efb55c6e&format=1'
  belongs_to :coin

  #This method checks if the value given for the coin_id and date given by parameters exists and, if it does not,
  #it calculates it.
  #Then it returns the value for that date.
  #It uses different endpoints to the API call depending if the coin is a cryptocurrency or not.
  def self.get_value_by_date(coin_id, date = nil)
    if date == nil
      date = Date.today
    end

    @cv = CoinValue.where(coin: coin_id).where(date: date)
      if not @cv.present?
        @coin = Coin.find(coin_id)
        if @coin.is_crypto
          api_url = self.url_api_crypto(date, @coin.short_name)
          data = JSON.parse response = open(api_url).read
          price = data[@coin.short_name]['USD']
        else
          api_url = self.url_api_regular(date, @coin.short_name)
          data = JSON.parse response = open(api_url).read
          index = "USD" << @coin.short_name
          price = data["quotes"][index]
        end
        @cv = CoinValue.create(date: date, price: price, coin_id: coin_id)
    else
      @cv = @cv.first
    end

    @cv.price
    end

  #Gets the value of the coin for the date range given
  def self.fill_dates(coin_short_name, starting_date, ending_date)
    values = {}
    @coin = Coin.where(short_name: coin_short_name).first
    (starting_date..ending_date).each do |day|
      values[day] = self.get_value_by_date(@coin.id, day)
    end
    values
  end

  #Calculates the predicted value for the coin by doing the average of the last 7 values of this coin
  def self.predict(coin_short_name)
    from_date = Date.today - 7
    to_date = Date.today
    last_7_days = self.fill_dates(coin_short_name, from_date, to_date)
    last_7_days.values.sum / 7
  end

  def self.url_api_regular(date, coin)
    out = CoinValue::URL_API_REGULAR + '&date=' + date.to_s
    out + '&currencies=' + coin + "&format=1"
  end

  def self.url_api_crypto(date, coin)
    CoinValue::URL_API_CRYPTO + "fsym=" << coin + "&tsyms=USD,ts=" + date.to_time.to_i.to_s
  end
end