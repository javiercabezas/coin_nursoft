class Coin < ApplicationRecord

  #Returns the name of the coin and the value for today.
  def to_h
    {
        name: name,
        value: CoinValue.get_value_by_date(id)
    }
  end

  def to_s
    name
  end


end